---
title: HIFIS Services
title_image: default
layout: default
author: none
redirect_from:
  - services/backbone/index_eng.html
  - services/backbone/index_ger.html
  - services/backbone
  - mission/cluster-backbone.html
  - services/cloud/index_eng.html
  - services/cloud/index_ger.html
  - services/cloud
  - mission/cluster-cloud.html
  - services/software/index_eng.html
  - services/software/index_ger.html
  - services/software
  - mission/cluster-software.html
---

HIFIS ensures an excellent information environment for outstanding research
in all Helmholtz research fields. 

## Software
The service portfolio of HIFIS Software is structured into 4 different
components that seamlessly interoperate with each other:
Education & Training, Technology, Consulting and Community Services.

<div class="flex-cards">
{%- assign posts = site.pages | where_exp: "item", "item.path contains 'services/software'" -%}
{% for post in posts -%}
{% include cards/post_card_image.html post=post excerpt=true %}
{% endfor -%}
</div>

## Cloud
In the Helmholtz Cloud, members of the Helmholtz Association of German Research Centres provide selected IT-Services for joint use.

<div class="flex-cards">
{%- assign posts = site.pages | where_exp: "item", "item.path contains 'services/cloud'" -%}
{% for post in posts -%}
{% include cards/post_card_image.html post=post excerpt=true %}
{% endfor -%}
</div>

## Backbone
High-performance trusted network infrastructure with unified basic services.

<div class="flex-cards">
{%- assign posts = site.pages | where_exp: "item", "item.path contains 'services/backbone'" -%}
{% for post in posts -%}
{% include cards/post_card_image.html post=post excerpt=true %}
{% endfor -%}
</div>

## Overall
<div class="flex-cards">
{%- assign posts = site.pages | where_exp: "item", "item.path contains 'services/overall'" -%}
{% for post in posts -%}
{% include cards/post_card_image.html post=post excerpt=true %}
{% endfor -%}
</div>
