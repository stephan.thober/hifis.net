---
title: Cloud Services & Portfolio
title_image: halgatewood-com-tZc3vjPCk-Q-unsplash.jpg
layout: services/default
author: none
additional_css:
    - services/services-page-images.css
excerpt: >-
    More information about Helmholtz Cloud and its portfolio.
---

<p>
    Helmholtz Cloud is a federated cloud platform and offers services to all Helmholtz centres and the entire scientific community and partners. The cloud service portfolio primarily consists of existing heterogeneous solutions of the different centres (providers) and, where possible and reasonable, establishes a “meta-level” over these. 
</p>

<p>    
The provision of Helmholtz Cloud Services requires technical and organisational frameworks to open up a broad portfolio of services to all users. 
</p>

<h1>Helmholtz Cloud Service Portfolio</h1>
<div class="image-block">
    <img
        class="help-image right"
        alt="Someone working with laptop on a stack of IT devices"
        src="{{ site.directory.images | relative_url }}/services/undraw_Maintenance_re_59vn.svg"
    />
    <div>
        <p>
        The <a href="https://www.hifis.net/doc/service-portfolio/initial-service-portfolio/how-services-are-selected/">initial Service Selection Process</a> has been worked out to efficiently identify the services which fulfill the selection criteria best and which should consequently be integrated into Helmholtz cloud first. 
        </p>
        <p>
        At the end of this process, <a href="https://www.hifis.net/doc/service-portfolio/initial-service-portfolio/how-services-are-selected/#selected-services-for-initial-helmholtz-cloud-service-portfolio">38 Helmholtz Cloud Services</a> offered by nine Helmholtz Centres were selected. Considering that some services are provided by several centres, the initial service portfolio includes 21 different services.
        </p>
        <p>
        20 of them are already available on the <a href="https://cloud.helmholtz.de/">Helmholtz Cloud Portal</a>.
        </p>
    </div>
</div>
<div class="image-attrib">Images by Katerina Limpitsouni via <a href="https://undraw.co/">undraw.co</a></div>

### Upcoming Services

The portfolio is continuously being updated.
[The current status can be found here](https://hifis.net/doc/service-portfolio/service-portfolio-management/current-services-in-portfolio/).

Next upcoming services will presumably be:

* Rancher managed Kubernetes by DESY
* Redmine by HMGU
* Singularity (on HAICORE) by KIT

<div>
<p>
        <a href="https://www.hifis.net/doc/service-portfolio/" type="button" class="btn btn-outline-secondary btn-sm">
        <i class="fas fa-file-alt"></i> Documentation
        </a>
    </p>
</div>

### Process Framework

The Process Framework for Helmholtz Cloud Service Portfolio focuses on the explanation of the processes regarding the Service Portfolio Management for Helmholtz Cloud, giving an overview of which processes exist, how they interconnect, which roles are involved in each process and what is included in each process step. It serves as a guide to understand how Service Portfolio Management for Helmholtz Cloud works. 

<div>
<p>
        <a href="https://hifis.net/doc/process-framework/Chapter-Overview/" type="button" class="btn btn-outline-secondary btn-sm">
        <i class="fas fa-file-alt"></i> Process Framework
        </a>
    </p>
</div>

<h3>Further information</h3>
<div>
    <p>
    <a href="https://cloud.helmholtz.de">Helmholtz Cloud Portal</a>: Browse our current cloud services.
    </p>
    <p>
    <a href="Helmholtz_cloud.html">About the Helmholtz Cloud</a>
    </p>
    <p>
    <a href="cloud_login.html">Login and access to Helmholtz Cloud services</a>
    </p>
    <p>
    <a href="provider.html">Helmholtz Cloud Service Provider</a>
    </p>
</div>

<div>
<h3>Feedback and Support</h3>
    <p>
    We welcome your suggestions, tips and support. They help us to become better. Please contact us at <a href="mailto:{{site.contact_mail}}">{{site.contact_mail}}</a> directly.
    </p>
</div>

