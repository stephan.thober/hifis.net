---
title: Job Offers
title_image: default
layout: default
excerpt:
    Job offers related to HIFIS
redirect_from: jobs
---

# Job Offers related to HIFIS
{:.text-success}

### Helmholtz-Zentrum Berlin (HZB):

* [<i class="fas fa-external-link-alt"></i> **IT-Systems Engineer (Dev-Ops) (f/m/d)**](https://recruitingapp-5181.de.umantis.com/Vacancies/1567/Description/2?lang=eng)

  The Helmholtz-Zentrum Berlin für Materialien und Energie is responsible in HIFIS for the coordination and control of the cloud activities of the Helmholtz Association. We are looking for you to manage this extensive and innovative project together with us!

  (Link to [German Version](https://recruitingapp-5181.de.umantis.com/Vacancies/1567/Description/1?lang=ger).)

  Application deadline: Feb 06, 2022.

---

# Further offers

* [Please also have a look at further vacancies within the Helmholtz Association](https://www.helmholtz.de/en/jobs_talent/job_vacancies/)
