---
title: "Helmholtz Hacky Hour #31"
layout: event
organizers:
  - dolling
type:      hacky-hour
start:
  date:   "2021-06-30"
  time:   "14:00"
end:
  time:   "15:00"
location:
  campus: "Online"
  room:   "<a href=https://meet.gwdg.de/b/max-93j-2ef><i class=\"fas fa-external-link-alt\"></i> meet.gwdg.de</a>"
excerpt:  "<strong>When to parallelize your software</strong> It is running for three hours while only doing simple calculations..."
---
## When to parallelize your software
The Hacky Hour is intended to discuss and learn about different topics in the context of research software development. During the session, you will meet Helmholtz researchers from different fields and have the opportunity to present your favorite tools and techniques. If you want to join, share your experience or have any question on the topic, let us and others know about it in [the meta pad](https://pad.gwdg.de/0HczFKgqS_C9L1QGzfpbJA#).

Computers are capable of multi-tasking. When is it possible to make use of it, what are the problems, that one can encounter doing it, and what tools actually get the job(s) done?

We are looking forward to seeing you!
