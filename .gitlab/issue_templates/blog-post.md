/assign me
/label ~"blog post"

# Blog Post Suggestion

## Authors

> Name the involved authors here, maybe suggesting contributors.
> If you know their GitLab aliases, use @username to keep them
> involved.

## Reviewers

> Who do you want as reviewers of your Blog Post?

## Topic and Content

> Outline what you want to write about.
> Specify the style of your post (e.g. report, user story, guide, etc.).

## Target Audience

> Who is your blog post aimed at?
> Should these people have previous knowledge or skills?

## Publishing Date

> When is your preferred date to publish your Blog Post?
> 1. Add the date as text in this section
> 2. Replace <date> in the quick action below with the date to generate an
> automated entry in the GitLab Issue

/due <date>

## Contribution

> How does the content of your Blog Post contribute to the goals of
> HIFIS Software-Services?
