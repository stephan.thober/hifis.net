---
title: HIFIS SAB
title_image: default
layout: default
redirect_from:
  - mission/members-sab.html
  - mission/members-sab
excerpt:
    Detailed description of the HIFIS governance structures.
---

## Members of the Scientific Advisory Board
The Assembly of Members of the Helmholtz Association has installed the Scientific Advisory Board (SAB). 
The following internationally renowned scientists contribute to HIFIS's progress by their advice as SAB members:
* Ari Asmi; University of Helsinki
* Rosa M Badia; BSC
* Magchiel Bijsterbosch; SURF
* Michael Brünig; University of Queensland
* Isabel Campos; CSIC
* Tiziana Ferrari; EGI
* Andy Götz; ESRF
* Christian Grimm; DFN
* Frédéric Hemmer; CERN
* Marc Heron; DLS
* Mirjam van Daalen; PSI
* Jörg Herrmann; MPG
* Neil Chue Hong; SSI
* Christine Kirkpatrick; NDS, SDSC
* Rafael Laguna; Open-Xchange AG
* Rupert Lück; EMBL
* Pierre Etienne Macchi; IN2P3 CNRS
* Wolfgang E. Nagel; TU Dresden
* Davide Salomoni; INFN
* Bruno Weikl; FhG

