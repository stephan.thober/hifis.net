---
layout: spotlight

# Spotlight list attributes
name: SaQC
preview_image: saqc/SaQC-Image.png
excerpt: A consistent, extensible, easy-to-use tool/framework for reproducible quality control of time series data.

# Title for individual page
title_image: default
title: System for automated Quality Control - SaQC
keywords:
    - Time series
    - Quality control
    - Data analysis
hgf_research_field: Earth and Environment
hgf_centers:
    - UFZ Leipzig
contributing_organisations:
scientific_community:
impact_on_community:
contact: david.schaefer@ufz.de
platforms:
    - type: gitlab
      link_as: https://git.ufz.de/rdm-software/saqc
    - type: webpage
      link_as: https://rdm-software.pages.ufz.de/saqc/index.html
license: GPL v3
costs: free
software_type:
    - Data analysis
application_type:
    - Command line application
    - Python Module
programming_languages:
    - Python
doi:
funding:
    name: UFZ
    link_as: https://www.ufz.de
---

# SaQC in a nutshell

Anomalies and errors are the rule, not the exception when working with
time series data. This is especially true if such data originates
from <i>in situ</i> measurements of environmental properties.
Almost all applications, however, implicitly rely on data that complies
with some definition of 'correct'.

In order to infer reliable data products and tools, there is no alternative
to quality control. SaQC provides all the building blocks to comfortably
bridge the gap between 'usually faulty' and 'expected to be corrected' in
an accessible, consistent, objective and reproducible way.

<div class="spotlights-text-image">
<img src="{{ site.directory.images | relative_url}}spotlights/saqc/SaQC-Image.png" alt="SaQC">
<span>Exemplary screenshot of a time series analysis using SaQC.</span>
</div>
