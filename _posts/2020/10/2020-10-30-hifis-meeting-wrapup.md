---
title: "HIFIS Annual Meeting 2020: Wrap-up and materials"
title_image: omar-flores-MOO6k3RaiwE-unsplash.jpg
date: 2020-10-30
authors:
  - Schulz, Janne
  - jandt
layout: blogpost
categories:
  - news
excerpt: >
    On Wednesday 21st and Thursday 22nd October 2020, the second All-Hands HIFIS Meeting took place. It was carried out as a "two-day online video conference". Numerous members from all Helmholtz institutions were presented with a tightly planned and informative lecture programme. We provide materials and links to all aspects of HIFIS.

---

# Second All-Hands HIFIS meeting

On Wednesday 21st and Thursday 22nd October 2020, the second All-Hands HIFIS Meeting took place. Originally planned as a face-to-face event, the event was re-organized due to current developments and carried out as a "two-day online video conference". Numerous members from all Helmholtz institutions were presented with a tightly planned and informative lecture programme.
<!--more--> 

### Materials and Presentations
See here for the [detailed time plan as well as all presentation materials](https://events.hifis.net/event/25/timetable/#all.detailed).

### Day 1
Focus of the first day was on information about the current developments in the field of [Helmholtz Authentication and Authorization Infrastructure (AAI)](https://hifis.net/doc/backbone-aai/) and about the [Helmholtz Cloud Service selection process](https://hifis.net/doc/service-portfolio/initial-service-portfolio/how-services-are-selected/). 
The audience learned first-hand [which services](https://hifis.net/doc/service-portfolio/initial-service-portfolio/how-services-are-selected/#selected-services-for-initial-helmholtz-cloud-service-portfolio) are being integrated and offered in the first version of the Helmholtz Cloud. 

Subsequently, the audience was informed about the current developments in the area of HIFIS Software Services and further education in the use of scientific software development and publication. 

[In five breakout sessions](https://events.hifis.net/event/25/sessions/40/#20201021), participants were able to exchange views on questions such as "How does a federation of several Sync & Share services work" or "How will user requirements be incorporated into service portfolio management". In the summary that followed, the results of the breakout sessions were presented in a bundled form and discussed in the plenum.

### Day 2
The second day was dedicated to a number of special topics: 
The day started with a presentation on the current status of developments in the backbone working group, which aims to connect all Helmholtz institutions with a dedicated ["Helmholtz Backbone"]({% post_url 2020/09/2020-09-01-VPN %}). 
Based on this, the [HIFIS file transfer service](https://hifis.net/doc/core-services/fts-endpoint/) was presented, which will realize a secure and fast data transfer between the institutions.

In the presentation "[Consulting]({% link services/software/consulting.html %}) and Technology Services" the participants were informed about the experiences in terms of individual support in software development. 

The second part of the day was characterized by the presentation of the activities of the groups "Service Integration" and technical platform. 
A demonstration of an example workflow, which included several Helmholtz institutions, clearly showcased what is already possible. 
The second day was concluded with a demonstration of the current status of the Helmholtz Cloud service portal and a concluding plenary discussion.

### Feedback
The response to the two-day online meeting can be described as highly concentrated and constructively. The participants found the lectures consistently exciting and very informative. They were given a comprehensive and complete overview of the numerous developments in HIFIS.
