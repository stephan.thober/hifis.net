---
title: "HIFIS Software Helpdesk Launched"
title_image: default
date: 2020-06-01
authors:
  - huste
layout: blogpost
categories:
  - news
excerpt:
  The HIFIS Software Helpdesk has been launched.
  Using common HIFIS authorization and authentication infrastructure
  (future Helmholtz AAI), users can simply sign in using personal access data
  of their home institution or even their personal GitHub account.
lang: en
lang_ref: 2020-06-01-hifis-software-helpdesk-launched
---

The [HIFIS Software Helpdesk](https://{{ site.helpdesk }}) has been launched.
Using common HIFIS authorization and authentication infrastructure
(future Helmholtz AAI), users can simply sign in using personal access data
of their home institution or even their personal GitHub account.

<a type="button" class="btn btn-outline-primary btn-lg" href="{% post_url 2020/06/2020-06-01-helpdesk-launch %}">
  <i class="fas fa-external-link-alt"></i> Read more
</a>
